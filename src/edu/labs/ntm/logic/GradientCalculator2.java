package edu.labs.ntm.logic;

import ij.io.FileSaver;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

import java.awt.Point;
import java.awt.Polygon;
import java.io.File;
import java.util.ArrayList;

public class GradientCalculator2 {
	public static String PATH = System.getProperty("user.home") + "/Desktop/test";

	private OrthoReadImage blobImage, originalImg;

	private Polygon bonePoly;

	public GradientCalculator2(OrthoReadImage originalImg, OrthoReadImage blobImg, Polygon bonePoly, ArrayList<Polygon> callusPoly) {
		this.bonePoly = bonePoly;
		this.blobImage = blobImg;
		this.originalImg = originalImg;

		bonePoly.translate(1, 0);
	}

	public double calculateLocalRatio() {
		OrthoReadImage blobTemp = blobImage.copy();
		
		ImageConverter converter = new ImageConverter(blobTemp.getImage());
		converter.convertToGray8();
				
		OrthoReadImage callus = blobTemp.copy();
		OrthoReadImage background = blobTemp.copy();
		ImageProcessor callusProc = callus.getProcessor();
		ImageProcessor backgroundProc = background.getProcessor();
		int[][] originalArray, callusArray, backgroundArray;
		
		callusProc.threshold(10);
		backgroundProc.threshold(10);
		callus.setProcessor(callusProc);
		background.setProcessor(backgroundProc);
		
		callusArray = callusProc.getIntArray();
		
		backgroundProc = Morphology.dilate(background.getImage(), new StructElement("square", 11, false)).getProcessor().duplicate();
		background.setProcessor(backgroundProc);
		
		background = PixelProcessor.removePixels(callus, background).copy();
		
		backgroundArray = background.getProcessor().duplicate().getIntArray();
		
		for(int x=0; x<backgroundArray.length; x++)
			for(int y=0; y<backgroundArray[x].length; y++)
				if(backgroundArray[x][y] >= 240)
					if(bonePoly.contains(x, y))
						backgroundArray[x][y] = 0;
		
		backgroundProc.setIntArray(backgroundArray);
		background.setProcessor(backgroundProc);
		
		originalArray = originalImg.getProcessor().getIntArray();
		
		
		double bkgSum = 0, bkgTotal = 0, bkgAvg = 0;
		for(int x=0; x<originalArray.length; x++){
			for(int y=0; y<originalArray[x].length; y++){
				if(backgroundArray[x][y] >= 240){
					bkgSum += originalArray[x][y];
					bkgTotal++;
				}
			}
		}
		bkgAvg = (bkgSum/bkgTotal);		
		
		double calSum = 0, calTotal = 0, calAvg = 0;
		for(int x=0; x<originalArray.length; x++){
			for(int y=0; y<originalArray[x].length; y++){
				if(callusArray[x][y] >= 240){
					calSum += originalArray[x][y];
					calTotal++;
				}
			}
		}
		calAvg = (calSum/calTotal);
		
		System.err.println("Calratio: " + bkgAvg/calAvg);
		
		background.setTitle("Bkg");
		callus.setTitle("callus");
		background.show();
		callus.show();
		
		
		return 0;
	}
	
	public void saveImage(OrthoReadImage image, String name){
		File imgFile = new File(PATH+"");
		FileSaver.setJpegQuality(100);
		FileSaver saver = new FileSaver(image.getImage());
		saver.saveAsJpeg(imgFile.getPath() + name + ".jpg");
		
	}
}
