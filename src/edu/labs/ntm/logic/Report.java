package edu.labs.ntm.logic;

import ij.io.FileSaver;

import java.io.File;

public abstract class Report {
	public static String PATH = System.getProperty("user.home") + "/Desktop/test";

	/* Method to setup the PrintWriter to write out */
	protected abstract void initializePrintWriter();

	/* Method which will write data out */
	public abstract void writeTextFile();

	/* Method to add String data to write out */
	public abstract void add(String input);

	/* Method to add double data to write out */
	public abstract void add(double input);

	/* Method to close PrintWriter when done */
	public abstract void closeTextFile();

	public static void saveImage(OrthoReadImage image, String name){
		File imgFile = new File(PATH + "_images");
		
		if(!(imgFile.exists()))
			imgFile.mkdir();
		
		String savePath = imgFile.getPath() + "/" + name;
		FileSaver saver = new FileSaver(image.getImage());

		saver.saveAsJpeg(savePath + ".jpg");
	}

	public static void emptyImageDirectory(){
		File dirFile = new File(PATH + "_images");

		if(dirFile.exists()){
			File[] images = dirFile.listFiles();

			for(File i : images){
				i.delete();
			}
		}
	}
}
