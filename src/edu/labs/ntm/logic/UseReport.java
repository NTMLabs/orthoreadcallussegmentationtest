package edu.labs.ntm.logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import edu.labs.ntm.logic.test.Test;

/**
 * This class handles the report generation for OrthoRead
 * 
 * @author Stephen M. Porter
 */

public class UseReport extends Report{
	private final String format = "%-20s\t%-15s\t%-15s\t%-15s\n";

	private File file;
	private ArrayList<String> outList;
	private PrintWriter out;
	private boolean exists;

	public UseReport(){
		this.file = new File(PATH + "_rpt.txt");
		outList = new ArrayList<String>();
		exists = false;
		initializePrintWriter();
	}
	
	public UseReport(String path){
		this.file = new File(path + "_rpt.txt");
		outList = new ArrayList<String>();
		exists = false;
		initializePrintWriter();
	}

	@Override
	protected void initializePrintWriter() {
		try {
			if(file.exists()){
				file.delete();
				exists = true;
			}
			out = new PrintWriter(new BufferedWriter(new FileWriter(file.getPath(), true)));
		}

		catch (IOException e) {
			Test.log(Test.ERR, "Write out failed, IOException\n" + e);
		}
	}

	@Override
	public void writeTextFile() {
		if(exists){
			out.printf(format, "Filename", "Calculated", "Target", "% Error");
			exists = false;
		}
		
		out.printf(format, outList.get(0), outList.get(1), outList.get(2), outList.get(3));
		outList.clear();
	}

	@Override
	public void add(String input) {
		outList.add(input);
	}
	
	@Override
	public void add(double input) {
		outList.add(""+input);
	}
	
	@Override
	public void closeTextFile(){
		out.close();
	}
}
