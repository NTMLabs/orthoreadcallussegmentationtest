package edu.labs.ntm.logic;

import ij.ImagePlus;
import ij.measure.CurveFitter;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Polygon;

import plugins.Morphology.BinaryFill_2;

/**
 * This class is extended by CallusProcessor in order to perform specific, common
 * functions on pixels within the image being processed.
 * 
 * @author Stephen M. Porter
 */

public class PixelProcessor {
	protected final int WHITE = 255;
	protected final int TOLERANCE = 230;
	protected final int BLACK = 0;
	protected final int LEFT = 0;
	protected final int RIGHT = 1;
	protected final int TOP = 2;
	
	protected boolean filterProvided = false;
	
	protected int filter = -1;
	
	/**
	 * Disconnects callus not connected to the cortex. This is down by
	 * performing a morphological open followed by a close which removes
	 * isolated pixels
	 * 
	 * @param improc
	 *            The image processor to manipulate
	 * @return The new processor with the operation performed
	 */
	protected ImagePlus smoothOp(ImagePlus img, int radius) {
		return close(open(img, radius), radius);
	}
	
	/**
	 * @param image
	 */
	protected int countCallusPixels(ImagePlus image){
		ImageConverter ic = new ImageConverter(image);
		ImageProcessor blobProc;
		int[][] array;
		int sum = 0;

		ic.convertToGray8();
		blobProc = image.getProcessor().duplicate();
		blobProc.threshold(1);
		array = blobProc.getIntArray();

		for (int x = 0; x < array.length; x++) {
			for (int y = 0; y < array[x].length; y++) {
				if(array[x][y] == WHITE)
					sum++;
			}
		}
		
		return sum;
	}
	
	protected Polygon translate(Polygon callusPoly, int side){
		if (side == LEFT)
			callusPoly.translate(-5, 0);
		else if (side == RIGHT)
			callusPoly.translate(5, 0);
		else if (side == TOP)
			callusPoly.translate(0, -5);
		else
			callusPoly.translate(0, 5);
		
		return callusPoly;
	}
	
	protected Polygon reverseTranslate(Polygon callusPoly, int side){
		if (side == LEFT)
			callusPoly.translate(5, 0);
		else if (side == RIGHT)
			callusPoly.translate(-5, 0);
		else if (side == TOP)
			callusPoly.translate(0, 5);
		else
			callusPoly.translate(0, -5);
		
		return callusPoly;
	}

	/**
	 * Performs the morphological open (eroding then dilating pixels)
	 * 
	 * @param improc
	 *            The image processor to manipulate
	 * @return The new processor with the open performed
	 */
	protected ImagePlus open(ImagePlus img, int radius) {
		return Morphology.open(img, new StructElement("circle", radius, false));
	}

	/**
	 * Performs the morphological close (dilating then eroding pixels)
	 * 
	 * @param improc
	 *            The image processor to manipulate
	 * @return The new processor with the close performed
	 */
	protected ImagePlus close(ImagePlus img, int radius) {
		return Morphology.close(img, new StructElement("circle", radius, false));
	}

	/**
	 * Filter specific gradient values based on specified threshold
	 * 
	 * @param gradientArray
	 * @param thresh
	 * @return
	 */
	protected int[][] filterGradient(int[][] gradientArray, int thresh, double mean, double stdDev) {
		
		if(filter == -1){
			if(thresh > 5)
				filter = 10;
			else if(thresh == 5)
				filter = thresh;
			else
				filter = (thresh + (thresh / 2));
		}
		
		filter = 40;
		

		for (int x = 0; x < gradientArray.length; x++) {
			for (int y = 0; y < gradientArray[x].length; y++) {
					if (gradientArray[x][y] >= filter)
						gradientArray[x][y] = 255;
			}
		}
		
		return gradientArray;
	}
	
	public static OrthoReadImage removePixels(OrthoReadImage remover, OrthoReadImage removee){
		ImageProcessor removerProc, removeeProc;
		int[][] removerArray, removeeArray;
		
		removerProc = remover.getProcessor();
		removeeProc = removee.getProcessor();
		
		removerArray = removerProc.getIntArray();
		removeeArray = removeeProc.getIntArray();
		
		for(int x=0; x<removeeArray.length; x++)
			for(int y=0; y<removeeArray[x].length; y++)
				if(removerArray[x][y] == 255)
					removeeArray[x][y] = 0;
		
		removeeProc.setIntArray(removeeArray);
		removee.setProcessor(removeeProc);
		
		return removee;
	}

	/**
	 * Fill holes in objects contained in an image
	 * 
	 * @param improc
	 * @return
	 */
	protected ImageProcessor fillHoles(ImageProcessor improc) {
		BinaryFill_2 bf = new BinaryFill_2();
		bf.run(improc);
		return improc;
	}

	/**
	 * Determines the orientation of the bone cortex
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	protected boolean isVertical(int x1, int y1, int x2, int y2, int width, int height) {
		double[] xpoints = {x1, x2};
		double[] ypoints = {y1, y2};
		double[] xedge = {0, width-1};
		double[] yedge = {height-1, 0};
		
		CurveFitter curve1 = new CurveFitter(xpoints, ypoints);
		curve1.doFit(CurveFitter.STRAIGHT_LINE);
		
		CurveFitter rot = new CurveFitter(xedge, yedge);
		rot.doFit(CurveFitter.STRAIGHT_LINE);
		
		double[] params = curve1.getParams();
		if(params[1] == 0 && (Math.abs(y1-y2)) > (Math.abs(x1-x2)))
			params[1] = Math.tan((89.99999) * (Math.PI/180));
		
		
		double rotation = Math.abs((Math.atan(rot.getParams()[1]) * (180/Math.PI)));
		double slope = Math.abs((Math.atan(params[1]) * (180/Math.PI)));
		
		if((slope > rotation && slope < (180-rotation)) || (slope > (180+rotation) && slope < (360-rotation)))
			return true;
		else
			return false;
	}

	/**
	 * Determines which side of the cortex the bone is on
	 * 
	 * @param improc
	 * @param isVertical
	 * @return
	 */
	protected int getSide(ImageProcessor improc, boolean isVertical) {
		int side = 0;
		int[][] pixels = improc.getIntArray();

		// Bone rotation is vertical
		if (isVertical) {
			int leftTotal = 0, rightTotal = 0;
			for (int x = 0; x <= 5; x++) {
				for (int y = 0; y < improc.getHeight(); y++) {
					leftTotal += pixels[x][y];
				}
			}

			for (int x = improc.getWidth() - 1; x >= improc.getWidth() - 6; x--) {
				for (int y = 0; y < improc.getHeight(); y++) {
					rightTotal += pixels[x][y];
				}
			}

			if (leftTotal > rightTotal)
				side = 0;
			else
				side = 1;
		}

		// Bone rotation is horizontal
		else {
			int topTotal = 0, botTotal = 0;
			for (int x = 0; x < improc.getWidth(); x++) {
				for (int y = 0; y <= 5; y++) {
					topTotal += pixels[x][y];
				}
			}

			for (int x = 0; x < improc.getWidth(); x++) {
				for (int y = improc.getHeight() - 1; y >= improc.getHeight() - 6; y--) {
					botTotal += pixels[x][y];
				}
			}

			if (topTotal > botTotal)
				side = 2;
			else
				side = 3;
		}
		return side;
	}

	/**
	 * Set all the pixels black
	 * 
	 * @param improc
	 * @return
	 */
	protected ImageProcessor setAllBlack(ImageProcessor improc) {
		improc.setColor(Color.BLACK);
		improc.fill();

		return improc;
	}
	
	/**
	 * @param array
	 * @param boneFill
	 * @param value
	 * @return
	 */
	protected int[][] processBone(int[][] array, int[][] boneFill, int value){
		for(int x=0; x<array.length; x++){
			for(int y=0; y<array[x].length; y++){
				if(boneFill[x][y] >= (TOLERANCE))
					array[x][y] = value;
			}
		}
		return array;
	}
	
	protected void setFilter(int value){
		filter = value;
	}
	
	public int getFilter(){
		return filter;
	}

	/**
	 * Overlays an image on top of another image using it's ImageProcessor
	 * starting at (0, 0)
	 * 
	 * @param originalProc
	 *            base image
	 * @param unitProc
	 *            image to overlay
	 * @return An image processor containing the overlayed image
	 */
	public static ImageProcessor overlay(ImageProcessor originalProc,
			ImageProcessor unitProc) {
		int[][] originalArray = originalProc.getIntArray();
		int[][] unitArray = unitProc.getIntArray();

		for (int x = 0; x < unitArray.length; x++)
			for (int y = 0; y < unitArray[x].length; y++)
				originalArray[x][y] = unitArray[x][y];

		originalProc.setIntArray(originalArray);
		return originalProc;
	}

}
