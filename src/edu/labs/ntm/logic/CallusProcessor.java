package edu.labs.ntm.logic;

import ij.blob.ManyBlobs;
import ij.gui.PolygonRoi;
import ij.gui.Roi;
import ij.measure.CurveFitter;
import ij.plugin.filter.RankFilters;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;

import java.awt.Color;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.HashMap;

import livewire.Dijkstraheap;
import edu.labs.ntm.logic.OrthoReadImage;

/**
 * CallusProcessor is a class which performs the necessary functions to segment
 * the cortex and callus growth on a fracture in an X-Ray image.
 * 
 * @author Stephen M. Porter
 */

public class CallusProcessor extends PixelProcessor {

	//=======================================================================//
	//--------------------------- Instance Data -----------------------------//
	//=======================================================================//
	//TODO REMOVE - Debug option to see images
	private final boolean SHOW_IMAGES = false;
	//////////////////////////////////////////
	
	
	private int x1Start, x2Start, y1Start, y2Start, x1End, y1End;
	private int x2End, y2End, x1Point, x2Point, y1Point, y2Point;
	private int x1Bridge, y1Bridge, x2Bridge, y2Bridge;
	private int callusPixelSum = 0;

	private ArrayList<Point> upperPoints;
	private ArrayList<Point> lowerPoints;
	private ArrayList<Point> cortexPoints;

	private HashMap<String, OrthoReadImage> imageMap;

	private boolean isVertical = true;
	private int side = LEFT;

	/**
	 * Constructor
	 */
	public CallusProcessor() {
		upperPoints = new ArrayList<Point>();
		lowerPoints = new ArrayList<Point>();
		cortexPoints = new ArrayList<Point>();
		imageMap = new HashMap<String, OrthoReadImage>();
	}

	// =======================================================================//
	// -------------------------- Private Methods ----------------------------//
	// =======================================================================//
	/**
	 * Method which fills in the bone side used to factor out the bone during the
	 * segmentation process
	 */
	private void boneFill() {
		OrthoReadImage bwBoneFill = new OrthoReadImage(imageMap.get("bwCortex").duplicate());
		ImageProcessor boneFillProc = bwBoneFill.getProcessor();
		int width = boneFillProc.getWidth();
		int height = boneFillProc.getHeight();

		/* Add the bridge to close the bone section for filling */
		boneFillProc.setColor(Color.WHITE);

		if (side == LEFT) {
			boneFillProc.drawLine(x1End, y1End, 0, y1End);
			boneFillProc.drawLine(x2End, y2End, 0, y2End);
			boneFillProc.drawLine(0, y1End, 0, y2End);
		}

		else if (side == RIGHT) {
			boneFillProc.drawLine(x1End, y1End, boneFillProc.getWidth() - 1, y1End);
			boneFillProc.drawLine(x2End, y2End, boneFillProc.getWidth() - 1, y2End);
			boneFillProc.drawLine(width - 1, y1End, width - 1, y2End);
		}

		else if (side == TOP) {
			boneFillProc.drawLine(x1End, y1End, x1End, 0);
			boneFillProc.drawLine(x2End, y2End, x2End, 0);
			boneFillProc.drawLine(x1End, 0, x2End, 0);
		}

		else {
			boneFillProc.drawLine(x1End, y1End, x1End, boneFillProc.getHeight() - 1);
			boneFillProc.drawLine(x2End, y2End, x2End, boneFillProc.getHeight() - 1);
			boneFillProc.drawLine(x1End, height - 1, x2End, height - 1);
		}

		fillHoles(boneFillProc);
		store(bwBoneFill, boneFillProc, "bwBoneFill");

		//TODO bwBoneFill
		if(SHOW_IMAGES){
			bwBoneFill.setTitle("bwBoneFill");
			bwBoneFill.show();
		}
		////////////////
	}

	/**
	 * Method which fills in the outlined callus.
	 */
	private void callusFill() {
		OrthoReadImage bwCallusFill = new OrthoReadImage(imageMap.get("bwCallusGrad").duplicate());
		OrthoReadImage bwBoneFill = new OrthoReadImage(imageMap.get("bwBoneFill").duplicate());
		ImageProcessor callusFillProc = bwCallusFill.getProcessor();
		int[][] bwBoneFillArray = bwBoneFill.getProcessor().getIntArray();

		callusFillProc.threshold(250);
		bwCallusFill.setProcessor(callusFillProc);
		bwCallusFill.setImage(close(bwCallusFill.getImage(), 4));

		//TODO Closing Operation
		if(SHOW_IMAGES){
			OrthoReadImage temp = new OrthoReadImage(bwCallusFill.duplicate());
			temp.setTitle("Closing Operation");
			temp.show();
		}
		/////////////

		callusFillProc = bwCallusFill.getProcessor();		
		bwCallusFill.setProcessor(fillHoles(callusFillProc));

		//TODO bwCallusFill
		if(SHOW_IMAGES){
			OrthoReadImage temp2 = new OrthoReadImage(bwCallusFill.duplicate());
			temp2.setTitle("bwCallusFill");
			temp2.show();
		}
		///////////////

		isolateCallus(callusFillProc, bwCallusFill, bwBoneFillArray);
	}

	/**
	 * Method which filters the gradient based on dynamic threshold
	 */
	private void callusGrad() {
		OrthoReadImage bwBoneFill = new OrthoReadImage(imageMap.get("bwBoneFill").duplicate());
		OrthoReadImage bwCallusGrad = new OrthoReadImage(imageMap.get("imgRoiFilt").duplicate());
		ImageProcessor improc = bwCallusGrad.getProcessor();
		ImageStatistics imgStats;
		int[][] bwBoneFillArray = bwBoneFill.getProcessor().getIntArray();
		int[][] bwCallusArray;
		int[][] gradientArray;
		int thresh;

		improc.findEdges();

		//TODO Gradient Image
		if(SHOW_IMAGES){
			OrthoReadImage temp = new OrthoReadImage(bwBoneFill.duplicate());
			temp.setProcessor(improc.duplicate());
			temp.setTitle("Gradient Image");
			temp.show();
		}
		//////////////

		gradientArray = improc.getIntArray();
		improc.setIntArray(processBone(gradientArray, bwBoneFillArray, BLACK));
		improc.sharpen();

		/* Filter based on threshold */
		imgStats = improc.getStatistics();
		thresh = improc.getAutoThreshold();
		improc.setIntArray(filterGradient(gradientArray, thresh, imgStats.mean, imgStats.stdDev));

		bwCallusArray = improc.getIntArray();
		improc.setIntArray(processBone(bwCallusArray, bwBoneFillArray, WHITE));

		store(bwCallusGrad, improc, "bwCallusGrad");

		//TODO bwCallusGrad
		if(SHOW_IMAGES){
			bwCallusGrad.getImage().setTitle("bwCallusGrad");
			bwCallusGrad.show();
		}
		////////////////////
	}

	/**
	 * Method which isolates the callus from the bone and background. Called
	 * after segmenting from callusFill.
	 * 
	 * @param improc
	 *            Processor used in callusFill
	 * @param image
	 *            callusFill image
	 * @param bwBoneFillArray
	 *            Array containing the pixel values for the bone
	 */
	private void isolateCallus(ImageProcessor improc, OrthoReadImage image, int[][] bwBoneFillArray) {
		OrthoReadImage bwCallusIso = image;
		OrthoReadImage imgCallusIso = new OrthoReadImage(imageMap.get("imgRoi").duplicate());
		ImageProcessor isoProc = improc;
		ImageProcessor imgIsoProc = imgCallusIso.getProcessor();
		ManyBlobs blobs;
		Polygon callusPoly = null;
		int[][] boneCallusPixelArray;

		/* Invert for blobs library; assumes background is white */
		isoProc.invert();
		boneCallusPixelArray = isoProc.getIntArray();
		isoProc.setIntArray(processBone(boneCallusPixelArray, bwBoneFillArray, WHITE));

		bwCallusIso.setProcessor(isoProc);
		bwCallusIso.setImage(smoothOp(bwCallusIso.getImage(), 3));

		//TODO Open/Close
		if(SHOW_IMAGES){
			OrthoReadImage temp = new OrthoReadImage(bwCallusIso.duplicate());
			temp.setTitle("Open/Close");
			temp.show();
		}
		//////////////

		blobs = new ManyBlobs(bwCallusIso.getImage());
		blobs.findConnectedComponents();

		/* Outline our callus */
		boolean onCortex;
		callusPixelSum = 0;
		for (int i = 0; i < blobs.size(); i++) {
			onCortex = false;
			callusPoly = blobs.get(i).getOuterContour();
			translate(callusPoly, side);

			for (int j = 0; j < cortexPoints.size(); j++) {
				if (callusPoly.contains(cortexPoints.get(j).x, cortexPoints.get(j).y)) {
					onCortex = true;
					break;
				}
			}

			if(onCortex){
				reverseTranslate(callusPoly, side);
				imgIsoProc.draw(new PolygonRoi(callusPoly, Roi.POLYLINE));
			}
		}

		/* Get the amount of pixels in the blob */
		callusPixelSum = countCallusPixels(blobs.getLabeledImage());

		/* Add the cortex and bridge outlines */
		imgIsoProc.setColor(Color.BLACK);
		imgIsoProc.setLineWidth(1);

		for(int i=0; i<cortexPoints.size(); i++){
			imgIsoProc.drawDot(cortexPoints.get(i).x, cortexPoints.get(i).y);
		}

		imgIsoProc.drawLine(x1Point, y1Point, x2Bridge, y2Bridge);
		imgIsoProc.drawLine(x2Point, y2Point, x1Bridge, y1Bridge);

		store(imgCallusIso, imgIsoProc, "imgCallusFinal");
	}

	/**
	 * Applies a median filter to the image to reduce noise
	 */
	private void medianFilter() {
		OrthoReadImage imgRoiFilt = new OrthoReadImage(imageMap.get("imgRoi").duplicate());
		ImageProcessor improc = imgRoiFilt.getProcessor().duplicate();
		RankFilters filter = new RankFilters();

		filter.rank(improc, 3.0, RankFilters.MEDIAN);

		store(imgRoiFilt, improc, "imgRoiFilt");

		//TODO imgRoiFilt
		if(SHOW_IMAGES){
			imgRoiFilt.setTitle("imgRoiFilt");
			imgRoiFilt.show();
		}
		////////////////////////////
	}

	/**
	 * This method finds the shortest distance between the two cortex selections
	 * and draws a line connecting them at those points. This is called from the
	 * outline cortex method and needs the ImageProcessor which contains the
	 * cortexes outlined in white.
	 * 
	 * @param bwCortexProc
	 *            ImageProcessor with the cortexes in white
	 */
	private void drawBridge() {
		OrthoReadImage bwCortex = new OrthoReadImage(imageMap.get("bwEdge").duplicate());
		ImageProcessor bwCortexProc = bwCortex.getProcessor();
		Point s1 = new Point(x1Bridge, y1Bridge);
		Point s2 = new Point(x2Bridge, y2Bridge);

		double shortestDistance = Double.MAX_VALUE;
		double temp = 0;
		
		for(int i=0; i<upperPoints.size(); i++){
			temp = s2.distance(upperPoints.get(i));
			
			if(temp < shortestDistance) {
				shortestDistance = temp;
				x1Point = upperPoints.get(i).x;
				y1Point = upperPoints.get(i).y;
			}
		}

		shortestDistance = Double.MAX_VALUE;
		temp = 0;
		for(int i=0; i<lowerPoints.size(); i++){
			temp = s1.distance(lowerPoints.get(i));

			if(temp < shortestDistance){
				shortestDistance = temp;
				x2Point = lowerPoints.get(i).x;
				y2Point = lowerPoints.get(i).y;
			}
		}

		bwCortexProc.setColor(Color.WHITE);
		bwCortexProc.drawLine(x1Point, y1Point, x2Bridge, y2Bridge);
		bwCortexProc.drawLine(x2Point, y2Point, x1Bridge, y1Bridge);
		bwCortexProc.erode();

		store(bwCortex, bwCortexProc, "bwCortex");

		//TODO bwCortex
		if(SHOW_IMAGES){
			bwCortex.getImage().setTitle("bwCortex");
			bwCortex.show();
		}
		/////////////////////////
	}

	private void interpolate(int height, int width){
		/* Prep points for curve fit */
		double[] x1points = {x1Start, x1End};
		double[] y1points = {y1Start, y1End};
		double[] x2points = {x2Start, x2End};
		double[] y2points = {y2Start, y2End};
		
		/* Get individual cortex orientations */
		boolean curve1IsVertical = isVertical(x1Start, y1Start, x1End, y1End, width, height);
		boolean curve2IsVertical = isVertical(x2Start, y2Start, x2End, y2End, width, height);

		/* Curve fit the points */
		CurveFitter curve1 = new CurveFitter(x1points, y1points);
		CurveFitter curve2 = new CurveFitter(x2points, y2points);

		curve1.doFit(CurveFitter.STRAIGHT_LINE);
		curve2.doFit(CurveFitter.STRAIGHT_LINE);

		/*Get curve fit paramaters*/
		double[] params1 = curve1.getParams();
		double[] params2 = curve2.getParams();

		if(curve1IsVertical){
			// y1End is closer to the bottom of the image
			if(Math.abs(y1End - (height - 1)) < y1End){
				y1End = (height - 1);

				if(params1[1] != 0)
					x1End = (int) ((y1End - params1[0]) / params1[1]);
			}

			else{
				y1End = 0;

				if(params1[1] != 0)
					x1End = (int) ((y1End - params1[0]) / params1[1]);
			}
		}

		else{
			// x1End is closer to the right edge of the image
			if(Math.abs(x1End - (width - 1)) < x1End){
				x1End = (width - 1);
				y1End = (int) curve1.f(params1, x1End);
			}

			else{
				x1End = 0;
				y1End = (int) curve1.f(params1, x1End);
			}
		}

		if(curve2IsVertical){			
			if(Math.abs(y2End - (height - 1)) < y2End){
				y2End = (height - 1);

				if(params2[1] != 0)
					x2End = (int) ((y2End - params2[0]) / params2[1]);
			}

			else{
				y2End = 0;

				if(params2[1] != 0)
					x2End = (int) ((y2End - params2[0] / params2[1]));
			}
		}

		else{
			if(Math.abs(x2End - (width - 1)) < x2End){
				x2End = (width - 1);
				y2End = (int) curve2.f(curve2.getParams(), x2End);
			}

			else{
				x2End = 0;
				y2End = (int) curve2.f(curve2.getParams(), x2End);
			}
		}
	}
	
	private void setupKeyPoints(ArrayList<Point> firstPoints, ArrayList<Point> secondPoints, int width, int height){
		x1Start = (int) firstPoints.get(firstPoints.size()-2).x;
		y1Start = (int) firstPoints.get(firstPoints.size()-2).y;
		x1End = (int) firstPoints.get(firstPoints.size()-1).x;
		y1End = (int) firstPoints.get(firstPoints.size()-1).y;
		
		x2Start = (int) secondPoints.get(secondPoints.size()-2).x;
		y2Start = (int) secondPoints.get(secondPoints.size()-2).y;
		x2End = (int) secondPoints.get(secondPoints.size()-1).x;
		y2End = (int) secondPoints.get(secondPoints.size()-1).y;
		
		x1Bridge = (int) firstPoints.get(0).x;
		y1Bridge = (int) firstPoints.get(0).y;
		x2Bridge = (int) secondPoints.get(0).x;
		y2Bridge = (int) secondPoints.get(0).y;
		
		interpolate(height, width);
	}

	/**
	 * Controller to run necessary operations in order
	 * to segment callus
	 * 
	 * @return Callus size in pixels
	 */
	private int findCallus(){
		drawBridge();
		medianFilter();
		boneFill();
		callusGrad();
		callusFill();

		return getCallusSize();
	}

	private void store(OrthoReadImage image, ImageProcessor improc, String key){
		image.setProcessor(improc);
		imageMap.put(key, image);
	}

	// =======================================================================//
	// --------------------------- Public Methods ----------------------------//
	// =======================================================================//
	/**
	 * Public access to begin callus semgmentation
	 * 
	 * @return Callus size in pixels
	 */
	public int findCallus(OrthoReadImage imageRoi) {
		store(imageRoi, imageRoi.getProcessor(), "imgRoi");
		return findCallus();
	}

	/**
	 * Segment cortex using the four user points
	 * 
	 * @param imageRoi Image ROI
	 * @param rois ArrayList of user selected PointRois
	 */
	public void selectCortex(OrthoReadImage imageRoi, ArrayList<Point> firstPoints, ArrayList<Point> secondPoints) {
		OrthoReadImage edgeImage = new OrthoReadImage(imageRoi.duplicate());
		OrthoReadImage cortexImage = new OrthoReadImage(imageRoi.duplicate());
		ImageProcessor edgeProc = edgeImage.getProcessor();
		ImageProcessor cortexProc = cortexImage.getProcessor();
		int width = imageRoi.getWidth();
		int height = imageRoi.getHeight();
		byte[] imageArray;
		
		/* Setup global key coordinates, interpolate end points, and add end points */
		setupKeyPoints(firstPoints, secondPoints, width, height);
		firstPoints.add(new Point(x1End, y1End));
		secondPoints.add(new Point(x2End, y2End));
		
		/* Setup key image properties */
		isVertical = isVertical(x1Start, y1Start, x1End, y1End, width, height);
		side = getSide(imageRoi.getProcessor(), isVertical);
		
		/* Get and process gradient image for cortex segmentation */
		edgeProc.findEdges();
		edgeProc.medianFilter();
		imageArray = (byte[]) edgeProc.duplicate().getPixels();

		/* Prep processor to draw cortexes */
		setAllBlack(edgeProc);
		edgeProc.setColor(Color.WHITE);
		
		/* Make heap for cortex processing */
		Dijkstraheap dijkstraHeap = new Dijkstraheap(imageArray, width, height);
		Point next, curr;
		next = new Point(0,0); 
		curr = new Point(0,0);
		
		/* Find the edge path of the first cortex */
		for(int i=firstPoints.size()-1; i>=1; i--){
			int[] vx = new int[width * height];
			int[] vy = new int[width * height];
			int[] size = new int[1];
			
			next.setLocation(firstPoints.get(i - 1));
			curr.setLocation(firstPoints.get(i));
			
			dijkstraHeap.setPoint(next.x, next.y);
			dijkstraHeap.run();

			while (size[0] < 1) {
				try {Thread.sleep(100);}
				catch (InterruptedException e) {}
				dijkstraHeap.returnPath(curr.x, curr.y, vx, vy, size);
			}
			
			/* Outline the cortex */
			for (int j = 0; j < size[0]; j++) {				
				Point p = new Point(vx[j], vy[j]);
				upperPoints.add(p);
				cortexPoints.add(p);
				edgeProc.drawPixel(p.x,p.y);
				cortexProc.drawPixel(p.x, p.y);
			}
		}
		
		for(int i=secondPoints.size()-1; i>=1; i--){
			int[] vx = new int[width * height];
			int[] vy = new int[width * height];
			int[] size = new int[1];

			next.setLocation(secondPoints.get(i - 1));
			curr.setLocation(secondPoints.get(i));
			
			dijkstraHeap.setPoint(next.x, next.y);
			dijkstraHeap.run();

			while (size[0] < 1) {
				try {Thread.sleep(100);}
				catch (InterruptedException e) {}
				dijkstraHeap.returnPath(curr.x, curr.y, vx, vy, size);
			}
			
			/* Outline the cortex */
			for (int j = 0; j < size[0]; j++) {
				Point p = new Point(vx[j], vy[j]);
				lowerPoints.add(p);
				cortexPoints.add(p);
				edgeProc.drawPixel(p.x,p.y);
				cortexProc.drawPixel(p.x, p.y);
			}
		}
		
		store(edgeImage, edgeProc, "bwEdge");
		store(cortexImage, cortexProc, "imgCortex");

		//TODO Edge Image
		if(SHOW_IMAGES){
			edgeImage.getImage().setTitle("edgeImage");
			cortexImage.getImage().setTitle("CortexImage");
			cortexImage.show();
			edgeImage.show();
		}
		/////////////
	}

	public OrthoReadImage getCortexOutline(){
		return imageMap.get("imgCortex");
	}

	public OrthoReadImage getFinalImage(){
		return imageMap.get("imgCallusFinal");
	}

	/**
	 * Getter to return callus size in pixels. Use
	 * only after using findCallus();
	 * 
	 * @return Pixel size of callus
	 */
	public int getCallusSize() {
		return callusPixelSum;
	}
	
	public void changeFilter(int value){
		setFilter(value);
		callusGrad();
		callusFill();
	}
}