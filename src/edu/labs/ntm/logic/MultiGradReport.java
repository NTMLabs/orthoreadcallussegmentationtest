package edu.labs.ntm.logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import edu.labs.ntm.logic.test.Test;

/**
 * This class handles the report generation for OrthoRead
 * 
 * @author Stephen M. Porter
 */

public class MultiGradReport {
//	private final String PATH = Report.PATH;

	private File file, file2;
	private PrintWriter out, out2;

	public MultiGradReport(String path){
		this.file = new File(path + "_gradRpt.txt");
		this.file2 = new File(path + "_gradSumRpt.txt");
		initializePrintWriter();
	}

	protected void initializePrintWriter() {
		try {
			if(file.exists())
				file.delete();
			
			
			if(file2.exists())
				file2.delete();
			
			
			out = new PrintWriter(new BufferedWriter(new FileWriter(file.getPath(), true)));
			out2 = new PrintWriter(new BufferedWriter(new FileWriter(file2.getPath(), true)));
		}

		catch (IOException e) {
			Test.log(Test.ERR, "Write out failed, IOException\n" + e);
		}
	}

	public void writeTextFile(ArrayList<GradReportObject> gradReportList, int sum, int positives, double ratio) {
		for(int i=0; i<gradReportList.size(); i++){
			out.println(gradReportList.get(i).toString());
		}
		out.println();
		out2.println(sum + "\t" + positives + "\t" + ratio);
	}
	
	public void closeFiles(){
		out.close();
		out2.close();
	}
}