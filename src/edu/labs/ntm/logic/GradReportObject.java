package edu.labs.ntm.logic;

import java.awt.Point;

public class GradReportObject {

	private int gradient;
	private double bkgAvg, calAvg;
	private Point loc1, loc2;
	private String dir;
	
	public GradReportObject(int gradient, Point loc1, Point loc2, String dir, double bkgAvg, double calAvg){
		this.gradient = gradient;
		this.loc1 = loc1;
		this.loc2 = loc2;
		this.dir = dir;
		this.bkgAvg = bkgAvg;
		this.calAvg = calAvg;
	}
	
	public int getGradient(){
		return gradient;
	}
	
	public Point getLoc1(){
		return loc1;
	}
	
	public Point getLoc2(){
		return loc2;
	}
	
	public String getDir(){
		return dir;
	}
	
	public String toString(){
		StringBuilder result = new StringBuilder();
		result.append("Gradient: " + getGradient() + "\t");
		result.append("Loc1: " + getLoc1().x + ", " + getLoc1().y + "\t");
		result.append("Loc2: " + getLoc2().x + ", " + getLoc2().y + "\t");
		result.append("Bkg: " + bkgAvg + "\t");
		result.append("Cal: " + calAvg + "\t");
		result.append("Direction: " + dir);
		
		return result.toString();
	}
}
