package edu.labs.ntm.logic;

import ij.io.FileSaver;
import ij.process.ImageProcessor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Handler class to manage IO operations
 * 
 * @author Stephen M. Porter
 */

@SuppressWarnings({ "rawtypes" })
public class FileHandler {

	/**
	 * Method to deserialize an arraylist on disc
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public ArrayList open(File file) throws FileNotFoundException,
			IOException, ClassNotFoundException {
		ObjectInputStream ois;
		ois = new ObjectInputStream(new FileInputStream(file));
		ArrayList list = (ArrayList) ois.readObject();
		ois.close();

		return list;
	}

	/**
	 * Method to serialize and save an arraylist to disk
	 * @param file
	 * @param saveList
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void save(File file, ArrayList saveList)
			throws FileNotFoundException, IOException {
		ObjectOutputStream oos;
		oos = new ObjectOutputStream(new FileOutputStream(file));
		oos.writeObject(saveList);
		oos.flush();
		oos.close();
	}

	/**
	 * Method to save OrthoReadImage objects as jpegs
	 * 
	 * @param imageProcessor
	 * @param filename
	 */
	public static void saveImage(OrthoReadImage image, ImageProcessor improc, String path) {
		FileSaver saver;
		FileSaver.setJpegQuality(100);
		image.setProcessor(improc);
		saver = new FileSaver(image.getImage());
		saver.saveAsJpeg(path);
	}
}
