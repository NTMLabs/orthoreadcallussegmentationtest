package edu.labs.ntm.logic;

import ij.ImagePlus;
import ij.gui.Roi;
import ij.io.Opener;
import ij.process.ImageProcessor;

import java.awt.Image;
import java.io.File;

public class OrthoReadImage {
//=======================================================================//
//--------------------------- Instance Data -----------------------------//
//=======================================================================//
	private ImagePlus image;
	private Opener opener = new Opener();
	private String filePath;
	
	/**
	 * Constructor
	 * 
	 * @param filePath String
	 */
	public OrthoReadImage(String filePath) {
		this.image = opener.openImage(filePath);
		this.filePath = filePath;
	}
	
	public OrthoReadImage(ImagePlus image){
		this.image = image;
	}
	
	public OrthoReadImage(File file){
		this.image = opener.openImage(file.getPath());
	}
	
//=======================================================================//
//-------------------------- Public Methods -----------------------------//
//=======================================================================//
	/**
	 * Method to close the image
	 * (no longer display it)
	 */
	public void close(){
		image.close();
	}
	
	/**
	 * Method to crop the image to the user determined ROI
	 * 
	 * @param imgProc ImageProcessor
	 * @param tempRoi Roi
	 * @param newRoi Roi
	 * @return
	 */
	public ImageProcessor crop(ImageProcessor imgProc, Roi newRoi){
			imgProc.setRoi(newRoi);
			imgProc = imgProc.crop();
			return imgProc;
	}
	
	public ImagePlus getImagePlus(){
		return image.duplicate();
	}
	
	/**
	 * Method to display the image
	 */
	public void show(){
		image.show();
	}
	
	/////////////
	// GETTERS //
	/////////////
	/**
	 * Returns the XImage's file path
	 * 
	 * @return String
	 */
	public String getFilePath(){
		return filePath;
	}
	
	/**
	 * Returns the height of the image
	 * @return int
	 */
	public int getHeight(){
		return image.getHeight();
	}
	
	/**
	 * Returns the ImagePlus container of this image
	 * 
	 * @return ImagePlus
	 */
	public ImagePlus getImage(){
		return image;
	}
	
	/**
	 * Returns an ImageProcessor for this image
	 * 
	 * @return ImageProcessor
	 */
	public ImageProcessor getProcessor(){
		return image.getProcessor().duplicate();
	}
	
	/**
	 * Returns the image ROI
	 * 
	 * @return Roi
	 */
	public Roi getRoi(){
		return image.getRoi();
	}
	
	/**
	 * Returns the total size of an image (the area)
	 * 
	 * @return
	 */
	public int getSize() {
		return (image.getHeight() * image.getWidth());
	}
	
	/**
	 * Returns the width of the image
	 * 
	 * @return int
	 */
	public int getWidth(){
		return image.getWidth();
	}
	
	/////////////
	// SETTERS //
	/////////////
	/**
	 * Sets the ImageX's image to be the specified image
	 * @param newImage Image
	 */
	public void setImage(Image newImage){
		image.setImage(newImage);
	}
	
	/**
	 * Sets the ImageX's image to be the specified image
	 * based on the file path of the image
	 * 
	 * @param filePath String
	 */
	public void setImage(String filePath){
		image = opener.openImage(filePath);
	}
	
	public void setImage(ImagePlus newImage){
		image = newImage;
	}
	
	/**
	 * Method to set the processor for the image
	 * 
	 * @param imageProcessor ImageProcessor
	 */
	public void setProcessor(ImageProcessor imageProcessor){
		image.setProcessor(imageProcessor);
	}
	
	public void setTitle(String title){
		image.setTitle(title);
	}
	
	public String getTitle(){
		return image.getTitle();
	}
	
	public ImagePlus duplicate(){
		return image.duplicate();
	}
	
	public OrthoReadImage copy(){
		return new OrthoReadImage(image.duplicate());
	}
}