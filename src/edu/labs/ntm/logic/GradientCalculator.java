package edu.labs.ntm.logic;

import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Point;
import java.awt.Polygon;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class GradientCalculator {
	public static String PATH = System.getProperty("user.home") + "/Desktop/test";

	private final int up = 0;
	private final int down = 1;
	private final int left = 2;
	private final int right = 3;

	private ArrayList<Point> points;
	private ArrayList<GradReportObject> gradList;
	private ArrayList<Polygon> callusPoly;
	private OrthoReadImage originalImg, blobImage;
	private int[][] origImgArray, blobArray;
	private Polygon bonePoly;
	private int[] x, y;
	private int npoints, height, width;

	public GradientCalculator(OrthoReadImage originalImg, OrthoReadImage blobImg, Polygon bonePoly, ArrayList<Polygon> callusPoly) {
		this.height = originalImg.getHeight() - 1;
		this.width = originalImg.getWidth() - 1;
		this.callusPoly = callusPoly;
		this.bonePoly = bonePoly;
		this.originalImg = originalImg;
		this.blobImage = blobImg;

		gradList = new ArrayList<GradReportObject>();
		origImgArray = originalImg.getProcessor().getIntArray();		
		blobArray = blobImg.getProcessor().getIntArray();
		
		points = new ArrayList<Point>();
		bonePoly.translate(1, 0);
	}

	public void calculate() {
		ArrayList<Integer> deleteList = new ArrayList<Integer>();
		
		for(int x=0; x<origImgArray.length; x++){
			for(int y=0; y<origImgArray[x].length; y++){
				if(origImgArray[x][y] <= 1){
					origImgArray[x][y] = 0;
				}
			}
		}
		
		for(int j=0; j<callusPoly.size(); j++){
			deleteList.clear();
			
			npoints = callusPoly.get(j).npoints;
			x = callusPoly.get(j).xpoints;
			y = callusPoly.get(j).ypoints;
			
			for(int i=0; i<x.length; i++){
				blobArray[x[i]][y[i]] = 0;
			}
			
			ImageProcessor tempProc = blobImage.getProcessor();
			tempProc.setIntArray(blobArray);
			blobImage.setProcessor(tempProc);
			saveBlobImage(blobImage.getImage());
			
			for(int i=0; i<npoints; i++){
				if(bonePoly.contains(x[i],y[i])){
					deleteList.add(i);
				}
			}
			
			x = delete(x, deleteList);
			y = delete(y, deleteList);
			

			for (int i = 0; i < x.length - 1; i++) {				
				int x1 = x[i];
				int x2 = x[i + 1];

				int y1 = y[i];
				int y2 = y[i + 1];
				
				
				if(x2 < 1)
					x2 = 1;

				if(y2 < 1)
					y2 = 1;
				
				
				if(x1 > width-1)
					x1 = width-1;
				if(x2 > width-1)
					x2 = width-1;

				if(y1 > height-1)
					y1 = height-1;
				if(y2 > height-1)
					y2 = height-1;
				
				/* Check if our points are horizontally connected */
				if ((x1 != x2) && (y1 == y2)) {

					/* Get the direction to calculate */
					if (blobArray[x1][y1-1] != 0)
						sumHoriz(down, i, (i + 1));
					else
						sumHoriz(up, i, (i + 1));
				}

				/* Check if our points are vertically connected */
				else if ((x1 == x2) && (y1 != y2)) {

					if (blobArray[x1-1][y1] != 0)
						sumVert(right, i, (i + 1));
					else
						sumVert(left, i, (i + 1));
				}

				/* Our pixels must be diagonally connected */
				else {
					int dir1, dir2;
					dir1 = dir2 = 0;

					/* See which of the 40 possible diagonals we have */
					if ((blobArray[x1][y1 + 1] > 1) && (blobArray[x2-1][y2]) > 1) {
						dir1 = down;
						dir2 = left;
					}
					
					else if((blobArray[x1][y1 + 1] > 1) && (blobArray[x2+1][y2]) > 1){
						dir1 = down;	
						dir2 = right;
					}
					
					else if ((blobArray[x1][y1 - 1] > 1) && (blobArray[x2-1][y2]) > 1){
						dir1 = up;
						dir2 = left;
					}
					
					else if ((blobArray[x1][y1 - 1] > 1) && (blobArray[x2 + 1][y2]) > 1){
						dir1 = up;
						dir2 = right;
					}
					
					else if ((blobArray[x1-1][y1] > 1) && (blobArray[x2][y2-1]) > 1){
						dir1 = left;
						dir2 = up;
					}
					
					else if ((blobArray[x1-1][y1] > 1) && (blobArray[x2][y2+1]) > 1){
						dir1 = left;
						dir2 = down;
					}
					
					else if ((blobArray[x1+1][y1] > 1) && (blobArray[x2][y2-1]) > 1){
						dir1 = right;
						dir2 = up;
					}
					
					else if ((blobArray[x1+1][y1] > 1) && (blobArray[x2][y2+1]) > 1){
						dir1 = right;
						dir2 = down;
					}

//					else if (blobArray[x1][y1 - 1] != 0) {
//						dir1 = up;
//						dir2 = left;
//						if (blobArray[x2 + 1][y2] != 0)
//							dir2 = right;
//					}
//
//					else if (blobArray[x1 - 1][y1] != 0) {
//						dir1 = left;
//						dir2 = up;
//						if (blobArray[x2][y2 + 1] != 0)
//							dir2 = down;
//					}
//
//					else {
//						dir1 = right;
//						dir2 = up;
//						if (blobArray[x2][y2 + 1] != 0)
//							dir2 = down;
//
//					}
					sumDiag(dir1, dir2, i, (i + 1));
				}
			}
		}
	}

	private void sumHoriz(int direction, int currIndex, int nextIndex) {
		Point p1 = checkPoint(new Point(x[currIndex], y[currIndex]));
		Point p2 = checkPoint(new Point(x[nextIndex], y[nextIndex]));
		String d = "";
		
		int yup1 = checkBounds(p1, up);
		int yup2 = checkBounds(p2, up);
		int ydown1 = checkBounds(p1, down);
		int ydown2 = checkBounds(p2, down);
		
		double calAvg = 0;
		double bkgAvg = 0;
		int gradient = 0;

		switch (direction) {
		case up:
			d = "U";
			calAvg = ((origImgArray[p1.x][ydown1] + origImgArray[p2.x][ydown2]) / 2);
			break;

		case down:
			d = "D";
			calAvg = ((origImgArray[p1.x][yup1] + origImgArray[p2.x][yup2]) / 2);
			break;
		}

		bkgAvg = ((origImgArray[p1.x][p1.y] + origImgArray[p2.x][p2.y]) / 2);
		gradient = ((int) (calAvg - bkgAvg));
		
		gradList.add(new GradReportObject(gradient, p1, p2, "Horizontal (" + d + ")" + " ", bkgAvg, calAvg));
	}

	private void sumVert(int direction, int currIndex, int nextIndex) {
		Point p1 = checkPoint(new Point(x[currIndex], y[currIndex]));
		Point p2 = checkPoint(new Point(x[nextIndex], y[nextIndex]));
		String d = "";
		
		int xright1 = checkBounds(p1, right);
		int xright2 = checkBounds(p2, right);
		int xleft1 = checkBounds(p1, left);
		int xleft2 = checkBounds(p2, left);
		
		double calAvg = 0;
		double bkgAvg = 0;
		int gradient = 0;

		switch (direction) {
		case right:
			d = "R";
			calAvg = ((origImgArray[xleft1][p1.y] + origImgArray[xleft2][p2.y]) / 2);
			break;

		case left:
			d = "L";
			calAvg = ((origImgArray[xright1][p1.y] + origImgArray[xright2][p2.y]) / 2);
			break;
		}

		bkgAvg = ((origImgArray[p1.x][p1.y] + origImgArray[p2.x][p2.y]) / 2);
		gradient = ((int) (calAvg - bkgAvg));

		gradList.add(new GradReportObject(gradient, p1, p2, "Vertical (" + d + ")", bkgAvg, calAvg));
	}

	private void sumDiag(int dir1, int dir2, int currIndex, int nextIndex) {
		Point p1 = checkPoint(new Point(x[currIndex], y[currIndex]));
		Point p2 = checkPoint(new Point(x[nextIndex], y[nextIndex]));
		
		//TODO REMOVE
		String d1, d2;
		d1 = d2 = "";
		
		int xright1 = checkBounds(p1, right);
		int xright2 = checkBounds(p2, right);
		int xleft1 = checkBounds(p1, left);
		int xleft2 = checkBounds(p2, left);
		
		int yup1 = checkBounds(p1, up);
		int yup2 = checkBounds(p2, up);
		int ydown1 = checkBounds(p1, down);
		int ydown2 = checkBounds(p2, down);
		
		double calAvg = 0;
		double bkgAvg = 0;
		int gradient = 0;

		switch(dir1){
		case down:
			d1 = "D";
			if(dir2 == left){
				d2 = "L";
				calAvg = (origImgArray[xleft2][ydown1]);
				bkgAvg = (origImgArray[xright1][yup2]);
			}else{
				d2 = "R";
				calAvg = (origImgArray[xright2][ydown1]);
				bkgAvg = (origImgArray[xleft1][yup2]);
			}break;

		case up:
			d1 = "U";
			if(dir2 == left){
				d2 = "L";
				calAvg = (origImgArray[xleft2][yup1]);
				bkgAvg = (origImgArray[xright1][ydown2]);
			}else{
				d2 = "R";
				calAvg = (origImgArray[xright2][yup1]);
				bkgAvg = (origImgArray[xleft1][ydown2]);
		}break;

		case left:
			d1 = "L";
			if(dir2 == down){
				d2 = "D";
				calAvg = (origImgArray[xleft1][ydown2]);
				bkgAvg = (origImgArray[xright2][yup1]);
			}else{
				d2 = "U";
				calAvg = (origImgArray[xleft1][yup2]);
				bkgAvg = (origImgArray[xright2][ydown1]);
			}break;

		case right:
			d1 = "R";
			if(dir2 == down){
				d2 = "D";
				calAvg = (origImgArray[xright1][ydown2]);
				bkgAvg = (origImgArray[xleft2][yup1]);
			}else{
				d2 = "U";
				calAvg = (origImgArray[xright1][yup2]);
				bkgAvg = (origImgArray[xleft2][ydown1]);
			}break;
		}

//		bkgAvg = (origImgArray[p1.x][p1.y]);
		gradient = ((int) (calAvg - bkgAvg));

		gradList.add(new GradReportObject(gradient, p1, p2, "Diag (" + d1 + "," + d2 + ")", bkgAvg, calAvg));
	}
	
	private int checkBounds(Point p, int direction){
		int result = 0;
		
		switch(direction){
		case up:
			if(p.y-1 < 0)
				result = 0;
			else
				result = p.y-1;
			break;
			
		case down:
			if(p.y+1 > height)
				result = height;
			else
				result = p.y+1;
			break;
			
		case left:
			if(p.x-1 < 0)
				result = 0;
			else
				result = p.x-1;
			break;
			
		case right:
			if(p.x+1 > width)
				result = width;
			else
				result = p.x+1;
			break;
		}
		
		return result;
	}
	
	private Point checkPoint(Point p){
		if(p.x < 0)
			p.x = 0;
		
		if(p.x > width)
			p.x = width;

		
		if(p.y < 0)
			p.y = 0;

		if(p.y > height)
			p.y = height;

		
		return p;
	}

	public ArrayList<GradReportObject> getGradList() {
		return gradList;
	}
	
	private int[] delete(int[] array, ArrayList<Integer> deleteList){
		ArrayList<Integer> temp = new ArrayList<Integer>();
		ArrayList<Integer> delList = deleteList;
		
		for(int i=0; i<delList.size(); i++){
			array[delList.get(i)] = 0;
		}
		
		for(int i=0; i<array.length; i++){
			temp.add(array[i]);
		}
				
		while(temp.contains(0)){
			for(int i=0; i<temp.size(); i++){
				if(temp.get(i) == 0){
					temp.remove(i);
				}
			}
		}
				
		int[] result = new int[temp.size()];
		
		for(int i=0; i<temp.size(); i++){
			result[i] = temp.get(i);
		}
		
		npoints = temp.size()-1;
		return result;
	}
	
	private void saveBlobImage(ImagePlus image){
		File imgFile = new File(PATH+"");
		FileSaver.setJpegQuality(100);
		FileSaver saver = new FileSaver(image);
		saver.saveAsJpeg(imgFile.getPath() + "blob.jpg");
		
	}
	
	public void saveImage(String name){
		File imgFile = new File(PATH + "");
		ImagePlus image = originalImg.duplicate();
		ImageProcessor proc = originalImg.getProcessor();
		proc.setColor(Color.BLACK);
		proc.fill();
		
		proc.setColor(Color.WHITE);
		for(int i=0; i<points.size(); i++){
			proc.drawPixel(points.get(i).x, points.get(i).y);
		}
		
		image.setProcessor(proc);
		
		String savePath = imgFile.getPath() + name;
		FileSaver saver = new FileSaver(image);

		saver.saveAsJpeg(savePath + ".jpg");
		points.clear();
	}
	
	public int getGradientSum(){
		int sum = 0;
		
		for(int i=0; i<gradList.size(); i++){
			sum += gradList.get(i).getGradient();
		}
		
		return sum;
	}
	
	public int getGradPositives(){
		int positives = 0;
		
		for(int i=0; i<gradList.size(); i++){
			if(gradList.get(i).getGradient() > 0)
				positives++;
		}
		
		return positives;
	}
	
	public int getPointCount(){
		return gradList.size();
	}
	
	private void addPoints(int[] x, int[] y){
		for(int i=0; i<x.length; i++){
			points.add(new Point(x[i], y[i]));
		}
	}
}
