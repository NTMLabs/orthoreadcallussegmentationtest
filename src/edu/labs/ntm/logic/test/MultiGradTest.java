package edu.labs.ntm.logic.test;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;

import edu.labs.ntm.logic.CallusProcessor2;
import edu.labs.ntm.logic.GradReportObject;
import edu.labs.ntm.logic.GradientCalculator;
import edu.labs.ntm.logic.MultiGradReport;
import edu.labs.ntm.logic.OrthoReadImage;
import edu.labs.ntm.logic.Report;

/**
 * 
 * 
 * @author Acadia
 */

public class MultiGradTest extends Test {
	private final String DEFAULT_PATH = System.getProperty("user.home") + "/Desktop/test";
	private String testImagePath, roiPointsPath, fileName, path;
	private ArrayList<ArrayList<Point>> pointList;
	private ArrayList<Point> firstPoints, secondPoints;
	private ArrayList<String> fileNames;
	private MultiGradReport report;
	private CallusProcessor2 callusProcessor;
	private int thresh, sampleRange = 20;
	private GradientCalculator gradCalc;

	protected void run(){
		setupPaths();
		execute();
		report.closeFiles();

		log("\nComplete test finished");
	}

	private void setupPaths(){
		Report.emptyImageDirectory();
		getFileNames();
		path = "Inputs/ImageRoi/";
	}

	private void execute(){
		for(int i=0; i<fileNames.size() - 1; i++){
			callusProcessor = new CallusProcessor2();
			fileName = fileNames.get(i);
			testImagePath = path + fileName;
			roiPointsPath = "Inputs/RoiFiles/" + getFileName(fileName);

			readFile();
			process(new OrthoReadImage(openImage(testImagePath)));

			if(report != null)
				report.closeFiles();
			report = new MultiGradReport(DEFAULT_PATH + "_pass_" + i);
			thresh = callusProcessor.getFilter();

			for(int j=1; j<=sampleRange; j++){
				thresh = j;
				callusProcessor.changeFilter(j);
				GradientCalculator gradCalc = new GradientCalculator(
						callusProcessor.getOriginalImg(), 
						callusProcessor.getBlobImg(), 
						callusProcessor.getBoneFill(), 
						callusProcessor.getCallusPoly());
				gradCalc.calculate();
				writeResults(gradCalc);
			}
		}
		report.closeFiles();
		pointList.clear();
		firstPoints.clear();
		secondPoints.clear();

		log("Completed: " + fileName + "\n");
	}

	@Override
	protected void readFile() {
		pointList = openPointFile(roiPointsPath);
		firstPoints = pointList.get(0);
		secondPoints = pointList.get(1);

	}

	@Override
	protected void process(OrthoReadImage image) {		
		log("Selecting cortex");
		callusProcessor.selectCortex(image, firstPoints, secondPoints);

		log("Finding callus");
		callusProcessor.findCallus(image);

	}

	@Override
	protected void writeResults() {

	}
	
	private void writeResults(GradientCalculator gradCalc){
		log("Printing results @ thresh: " + thresh);
		ArrayList<GradReportObject> list = gradCalc.getGradList();
		int sum = gradCalc.getGradientSum();
		int positives = gradCalc.getGradPositives();
		double ratio = ((double)positives)/gradCalc.getPointCount();
		report.writeTextFile(list, sum, positives, ratio);
//		gradCalc.saveImage(""+thresh);
		Report.saveImage(callusProcessor.getFinalImage(), fileName + "_" + thresh);
	}

	private void getFileNames(){
		fileNames = new ArrayList<String>();
		File[] files = new File("Inputs/ImageRoi").listFiles();

		for (File file : files) {
			if (file.isFile()) {
				fileNames.add(file.getName());
			}
		}
	}
}
