package edu.labs.ntm.logic.test;

import ij.io.FileSaver;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;

import edu.labs.ntm.logic.CallusProcessor;
import edu.labs.ntm.logic.OrthoReadImage;
import edu.labs.ntm.logic.Report;
import edu.labs.ntm.logic.UseReport;

/**
 * 
 * 
 * @author Acadia
 */

public class CompleteTest extends Test {
	private String testImagePath, roiPointsPath, clinicianPath, fileName, path;
	private ArrayList<ArrayList<Point>> pointList;
	private ArrayList<Point> firstPoints, secondPoints;
	private ArrayList<String> fileNames;
	private UseReport report;
	private CallusProcessor callusProcessor;
	int pass = 0;

	protected void run(){
		Report.emptyImageDirectory();
		getFileNames();
		path = "Inputs/ImageRoi/";
		report = new UseReport();

		for(int i=0; i<fileNames.size(); i++){
			callusProcessor = getProcessor();
			fileName = fileNames.get(i);
			testImagePath = path + fileName;
			roiPointsPath = "Inputs/RoiFiles/" + getFileName(fileName);
			clinicianPath = "Inputs/ClinicianResults/" + getFileName(fileName) + ".txt";

			readFile();
			process(new OrthoReadImage(openImage(testImagePath)));
			writeResults();

			pointList.clear();
			firstPoints.clear();
			secondPoints.clear();

			pass++;
			log("Completed: " + fileName + "\n");
		}
		report.closeTextFile();
		log("\nComplete test finished");
	}

	@Override
	protected void readFile() {
		pointList = openPointFile(roiPointsPath);
		firstPoints = pointList.get(0);
		secondPoints = pointList.get(1);

	}

	@Override
	protected void process(OrthoReadImage image) {		
		log("Selecting cortex");
		callusProcessor.selectCortex(image, firstPoints, secondPoints);

		log("Finding callus");
		callusProcessor.findCallus(image);
		
		log(Test.ERR, "Threshold: " + callusProcessor.getFilter());
	}

	@Override
	protected void writeResults() {
		log("Printing results");
		int callus = callusProcessor.getCallusSize();
		int clinic = Integer.parseInt(getClinicianResults(clinicianPath));
		
		report.add(fileName);
		report.add(callus);
		report.add(clinic);
		report.add(getPercentError(callus, clinic));
		report.writeTextFile();
		report.saveImage(callusProcessor.getFinalImage(), fileName);
	}

	private void getFileNames(){
		fileNames = new ArrayList<String>();
		File[] files = new File("Inputs/ImageRoi").listFiles();

		for (File file : files) {
			if (file.isFile()) {
				fileNames.add(file.getName());
			}
		}
	}
}
