package edu.labs.ntm.logic.test;

import java.awt.Point;
import java.util.ArrayList;

import edu.labs.ntm.logic.CallusProcessor2;
import edu.labs.ntm.logic.GradReportObject;
import edu.labs.ntm.logic.GradientCalculator;
import edu.labs.ntm.logic.GradientCalculator2;
import edu.labs.ntm.logic.OrthoReadImage;
import edu.labs.ntm.logic.Report;
import edu.labs.ntm.logic.UseReport;

public class SingleGradTest extends Test{
	private String testImagePath, roiPointsPath, clinicianPath, fileName;
	private ArrayList<Point> firstPoints, secondPoints;
	private UseReport report;
	private CallusProcessor2 callusProcessor;
	GradientCalculator2 c;
	
	public SingleGradTest(String path){
		report = new UseReport();
		testImagePath = path;
		roiPointsPath = "Inputs/RoiFiles/" + getFileName(path);
		fileName = getFileName(path);
		clinicianPath = "Inputs/ClinicianResults/" + getFileName(fileName) + ".txt";
		callusProcessor = new CallusProcessor2();
	}
	
	protected void run(){
		readFile();
		process(new OrthoReadImage(openImage(testImagePath)));
		writeResults();
		log(Test.OUT, "\nSingle test finished");
	}

	@Override
	protected void readFile() {
		ArrayList<ArrayList<Point>> pointList = openPointFile(roiPointsPath);
		firstPoints = pointList.get(0);
		secondPoints = pointList.get(1);
	}
	
	@Override
	protected void process(OrthoReadImage image) {
		log("Selecting cortex");
		callusProcessor.selectCortex(image, firstPoints, secondPoints);
		
		log("Finding callus");
		callusProcessor.findCallus(image);
		
		c = new GradientCalculator2(image, callusProcessor.getBlobImg(), callusProcessor.getBoneFill(), callusProcessor.getCallusPoly());
		c.calculateLocalRatio();
	}

	@Override
	protected void writeResults() {
		log("Printing results");
		
//		ArrayList<GradReportObject> list = c.getGradList();
//		for(int i=0; i<list.size(); i++){
//			System.out.println(list.get(i).toString());
//		}
//		System.out.println("Sum: " + c.getGradientSum());
//		System.out.println("Size: " + c.getGradList().size());
//		System.out.println(callusProcessor.getFilter());
//		c.saveImage("test");
//		Report.saveImage(callusProcessor.getFinalImage(), fileName);
	}
}
