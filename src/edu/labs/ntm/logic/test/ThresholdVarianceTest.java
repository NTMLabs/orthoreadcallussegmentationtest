package edu.labs.ntm.logic.test;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;

import edu.labs.ntm.logic.CallusProcessor;
import edu.labs.ntm.logic.OrthoReadImage;
import edu.labs.ntm.logic.Report;
import edu.labs.ntm.logic.ThresholdReport;

/**
 * 
 * 
 * @author Acadia
 */

public class ThresholdVarianceTest extends Test {
	private final String DEFAULT_PATH = System.getProperty("user.home") + "/Desktop/test";
	private String testImagePath, roiPointsPath, fileName, path;
	private ArrayList<ArrayList<Point>> pointList;
	private ArrayList<Point> firstPoints, secondPoints;
	private ArrayList<String> fileNames;
	private ThresholdReport report;
	private CallusProcessor callusProcessor;
	private int threshPass = 0, thresh, sampleRange = 20;

	protected void run(){
		setupPaths();
		execute();
		
		report.closeTextFile();
		log("\nComplete test finished");
	}
	
	private void setupPaths(){
		Report.emptyImageDirectory();
		getFileNames();
		path = "Inputs/ImageRoi/";
	}
	
	private void execute(){
		for(int i=0; i<fileNames.size(); i++){
			callusProcessor = new CallusProcessor();
			fileName = fileNames.get(i);
			testImagePath = path + fileName;
			roiPointsPath = "Inputs/RoiFiles/" + getFileName(fileName);

			readFile();
			process(new OrthoReadImage(openImage(testImagePath)));
			threshPass = 0;

			report = new ThresholdReport(DEFAULT_PATH + "_pass_" + i);
			thresh = callusProcessor.getFilter();
			
			for(int j=0; j<sampleRange + 2; j++){
				if(j==0){
					writeResults();
					threshPass++;
				}
				
				else{
					thresh = j-1;
					callusProcessor.changeFilter(j-1);
					writeResults();
					threshPass++;
				}
			}

			report.closeTextFile();

			pointList.clear();
			firstPoints.clear();
			secondPoints.clear();

			log("Completed: " + fileName + "\n");
		}
	}

	@Override
	protected void readFile() {
		pointList = openPointFile(roiPointsPath);
		firstPoints = pointList.get(0);
		secondPoints = pointList.get(1);

	}

	@Override
	protected void process(OrthoReadImage image) {		
		log("Selecting cortex");
		callusProcessor.selectCortex(image, firstPoints, secondPoints);

		log("Finding callus");
		callusProcessor.findCallus(image);

//		image = callusProcessor.getFinalImage();
//		FileSaver saver = new FileSaver(image.getImage());
//		saver.saveAsJpeg("Output/Images/" + getFileName(path + fileName) + ".jpg");
	}

	@Override
	protected void writeResults() {
		log("Printing results @ thresh: " + thresh);
		int callus = callusProcessor.getCallusSize();

		report.add(fileName + "_" + threshPass);
		report.add(thresh);
		report.add(callus);
		report.writeTextFile();
		report.saveImage(callusProcessor.getFinalImage(), getFileName(fileName) + "_" + threshPass);
	}

	private void getFileNames(){
		fileNames = new ArrayList<String>();
		File[] files = new File("Inputs/ImageRoi").listFiles();

		for (File file : files) {
			if (file.isFile()) {
				fileNames.add(file.getName());
			}
		}
	}
}
