package edu.labs.ntm.logic.test;

import ij.ImagePlus;
import ij.io.Opener;
import ij.process.ImageConverter;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

import edu.labs.ntm.logic.CallusProcessor;
import edu.labs.ntm.logic.FileHandler;
import edu.labs.ntm.logic.OrthoReadImage;

public abstract class Test {

	//=================================================//
	//                  Instance Data                  //
	//=================================================//
	public static final int OUT = 0;
	public static final int ERR = 1;

	private final FileHandler fileHandler = new FileHandler();
	private final DecimalFormat df = new DecimalFormat("#0.0");

	//=================================================//
	//                 Abstract Methods                //
	//=================================================//
	/** Begin the test */
	protected abstract void run();

	/** Read in necessary files */
	protected abstract void readFile();

	/** Process the image */
	protected abstract void process(OrthoReadImage image);

	/** Write out results */
	protected abstract void writeResults();


	//=================================================//
	//               Implemented Methods               //
	//=================================================//
	/**
	 * Opens and returns an ImagePlus based on passed
	 * in file path.
	 * 
	 * @param testImage Path to image
	 * @return ImagePlus
	 */
	protected ImagePlus openImage(String testImage){
		log(OUT, "Reading File " + testImage);

		Opener opener = new Opener();
		ImageConverter converter;

		ImagePlus image = opener.openImage(testImage);
		converter = new ImageConverter(image);
		converter.convertToGray8();

		log(OUT, "Returning Image " + testImage);
		return image;
	}

	/**
	 * Deserializes serialized file to obtain
	 * the cortex points for an image.
	 * 
	 * @param filePath Path to the file
	 * @return ArrayList containing point lists
	 */
	@SuppressWarnings("unchecked")
	protected ArrayList<ArrayList<Point>> openPointFile(String filePath){
		try {
			log(OUT, "Reading points file " + filePath);
			return fileHandler.open(new File(filePath));
		} 

		catch (Exception e) {
			log(Test.ERR, e.toString());
			System.exit(1);
		}

		//Never reached
		return null;
	}

	protected static void log(String logString){
		log(OUT, logString);
	}

	/**
	 * Log test process to console
	 * 
	 * @param code Test.ERR for errors
	 * 			   Test.OUT for regular
	 * 
	 * @param logString Message to log
	 */
	public static void log(int code, String logString){
		switch(code){
		case OUT:
			System.out.println(logString);
			break;

		case ERR:
			System.err.println(logString);
			break;

		default:
			System.out.println(logString);
			break;
		}
	}

	/**
	 * Getter to return the CallusProcessor used for 
	 * testing.
	 * 
	 * When a CallusProcessor is needed from  any 
	 * children, this method should be called to 
	 * obtain it.
	 * 
	 * e.g. getProcessor().findCallus(image);
	 * @return
	 */
	protected CallusProcessor getProcessor(){
		return new CallusProcessor();
	}

	protected String getFileName(String path){
		File file = new File(path);
		String fileName = file.getName().replaceAll(".png", "");
		fileName = fileName.replaceAll(".jpg", "");

		return fileName;
	}

	protected String getClinicianResults(String path){
		String result = "";

		try{
			Scanner scanner = new Scanner(new File(path));
			while(scanner.hasNext()){
				result = scanner.next();
			}
		}
		catch(IOException e){}
		return result;
	}

	protected String getPercentError(int callus, int clinic){
		if(clinic == 0)
			return "N/A";

		double error = Math.abs(callus - clinic);
		error = error/clinic;
		error = error*100;
		return df.format(error);
	}
}
