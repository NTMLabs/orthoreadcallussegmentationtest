package edu.labs.ntm.logic.test;

/**
 * TestController is a controller class which handles
 * individual test instances (complete or single).
 *  
 * @author Stephen M. Porter
 */
public class TestController {
	public final static int COMPLETE = 0;
	public final static int SINGLE = 1;
	public final static int THRESH = 2;
	public final static int SINGLE_GRAD = 3;
	public final static int MULTI_GRAD = 4;
	
	private Test completeTest, singleTest, threshTest, gradTest;
	private int test;
	private String path;
	
	public TestController(int test, String path){
		this.test = test;
		this.path = path;
	}
	
	/**
	 * Public method to start the TestDriver process 
	 * based on constructor input.
	 */
	public void run(){
		log(Test.OUT, "Starting test");
		begin();
	}
	
	/**
	 * Private method called by public run
	 * to start the test instance.
	 */
	private void begin(){
		switch(test){
		case COMPLETE:
			log(Test.OUT, "Running complete test");
			runCompleteTest();
			break;
			
		case SINGLE:
			if(path.isEmpty() || path == null){
				log(Test.ERR, "No path specified for single test");
				System.exit(1);
			}
			
			log(Test.OUT, "Running single test " + path);
			runSingleTest();
			break;
			
		case THRESH:
			log(Test.OUT, "Running threshold variance test");
			runThresholdVarianceTest();
			break;
		case SINGLE_GRAD:
			log(Test.OUT, "Running single gradient test");
			runGradTest(SINGLE_GRAD);
			break;
		case MULTI_GRAD:
			log(Test.OUT, "Running multi-threshold gradient test");
			runGradTest(MULTI_GRAD);
		}
	}
	
	/**
	 * Handle a complete test run
	 */
	private void runCompleteTest(){
		completeTest = new CompleteTest();
		completeTest.run();
	}
	
	/**
	 * Handle a single test run
	 */
	private void runSingleTest(){
		singleTest = new SingleTest(path);
		singleTest.run();
	}
	
	private void runThresholdVarianceTest(){
		threshTest = new ThresholdVarianceTest();
		threshTest.run();
	}
	
	private void runGradTest(int test){
		switch(test){
		case SINGLE_GRAD:
			gradTest = new SingleGradTest(path);
			break;
		case MULTI_GRAD:
			gradTest = new MultiGradTest();
		}
		
		gradTest.run();
	}
	
	/**
	 * Connects to Test's log to output
	 * to the console.
	 * 
	 * @param type Test.OUT or Test.ERR
	 * @param logString What to be printed
	 */
	private void log(int type, String logString){
		Test.log(type, logString);
	}	
}
