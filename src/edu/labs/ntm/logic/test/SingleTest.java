package edu.labs.ntm.logic.test;

import java.awt.Point;
import java.util.ArrayList;

import edu.labs.ntm.logic.CallusProcessor2;
import edu.labs.ntm.logic.OrthoReadImage;
import edu.labs.ntm.logic.Report;
import edu.labs.ntm.logic.UseReport;

public class SingleTest extends Test{
	private String testImagePath, roiPointsPath, clinicianPath, fileName;
	private ArrayList<Point> firstPoints, secondPoints;
	private UseReport report;
	private CallusProcessor2 callusProcessor;
	
	public SingleTest(String path){
		report = new UseReport();
		testImagePath = path;
		roiPointsPath = "Inputs/RoiFiles/" + getFileName(path);
		fileName = getFileName(path);
		clinicianPath = "Inputs/ClinicianResults/" + getFileName(fileName) + ".txt";
		callusProcessor = new CallusProcessor2();
	}
	
	protected void run(){
		readFile();
		process(new OrthoReadImage(openImage(testImagePath)));
		writeResults();
		log(Test.OUT, "\nSingle test finished");
	}

	@Override
	protected void readFile() {
		ArrayList<ArrayList<Point>> pointList = openPointFile(roiPointsPath);
		firstPoints = pointList.get(0);
		secondPoints = pointList.get(1);
	}
	
	@Override
	protected void process(OrthoReadImage image) {
		log("Selecting cortex");
		callusProcessor.selectCortex(image, firstPoints, secondPoints);
		
		log("Finding callus");
		callusProcessor.findCallus(image);
	}

	@Override
	protected void writeResults() {
		log("Printing results");
		int callus = callusProcessor.getCallusSize();
//		int clinic = Integer.parseInt(getClinicianResults(clinicianPath));
		
		report.add(fileName);
		report.add(callus);
		report.add(0); // clinic
		report.add(getPercentError(callus, 0)); // clinic
		report.writeTextFile();
		report.closeTextFile();
		Report.saveImage(callusProcessor.getFinalImage(), fileName);
	}
}
