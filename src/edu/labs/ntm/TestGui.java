package edu.labs.ntm;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import edu.labs.ntm.logic.Report;
import edu.labs.ntm.logic.test.Test;
import edu.labs.ntm.logic.test.TestController;
import edu.labs.ntm.logic.test.ThresholdVarianceTest;

@SuppressWarnings("serial")
public class TestGui extends JFrame implements ActionListener {
	private final Cursor WAIT_CURSOR = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private final Cursor DEFAULT_CURSOR = Cursor.getDefaultCursor();
	private final String DEFAULT_PATH = System.getProperty("user.home") + "/Desktop/test";
	
	private JPanel instructionPanel, buttonPanel;
	private JLabel instruction;
	private JButton complete, single, save, thresh, singleGrad, multiGrad;
	private JFileChooser fileChooser;
	
	public TestGui(){
		this.setSize(360, 135);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void run(){
		initialize();
		this.setVisible(true);
	}
	
	private void initialize(){
		setupLabels();
		setupButtons();
		setupFileChooser();
		setupPanel();
	}
	
	private void setupLabels(){
		instruction = new JLabel("Select a test to run");
	}
	
	private void setupButtons(){
		complete = new JButton("Complete");
		single = new JButton("Single");
		thresh = new JButton("Thresh");
		save = new JButton("Change Save Desitination");
		singleGrad = new JButton("Single Gradient");
		multiGrad = new JButton("Multi Gradient");
		
		
		complete.addActionListener(this);
		single.addActionListener(this);
		thresh.addActionListener(this);
		save.addActionListener(this);
		singleGrad.addActionListener(this);
		multiGrad.addActionListener(this);
	}
	
	private void setupFileChooser(){
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("Inputs/ImageRoi/"));
	}
	
	private void setupPanel(){
		buttonPanel = new JPanel(new GridLayout(2,3));
		instructionPanel = new JPanel();
		
		buttonPanel.add(complete);
		buttonPanel.add(single);
		buttonPanel.add(thresh);
		buttonPanel.add(singleGrad);
		buttonPanel.add(multiGrad);
		
		instructionPanel.add(instruction);
		
		this.add(instructionPanel, BorderLayout.CENTER);
		this.add(save, BorderLayout.NORTH);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == complete){
			runTest(TestController.COMPLETE, "");
		}
		
		else if(e.getSource() == single){
			runTest(TestController.SINGLE, getFilePath());
		}
		
		else if(e.getSource() == thresh){
			runTest(TestController.THRESH, "");
		}
		
		else if(e.getSource() == save){
			setDestination();
		}
		
		else if(e.getSource() == singleGrad){
			runTest(TestController.SINGLE_GRAD, getFilePath());
		}
		
		else if(e.getSource() == multiGrad){
			runTest(TestController.MULTI_GRAD, "");
		}
	}
	
	private void runTest(int testCode, String path){
		this.setCursor(WAIT_CURSOR);
		
		TestController td = new TestController(testCode, path);
		td.run();
		
		this.setCursor(DEFAULT_CURSOR);
		Report.PATH = DEFAULT_PATH;
	}
	
	private String getFilePath(){
		int choice = fileChooser.showOpenDialog(this);
		
		if(choice == JFileChooser.APPROVE_OPTION){
			return fileChooser.getSelectedFile().getPath();
		}
		
		return "";
	}
	
	private void setDestination(){
		int choice = fileChooser.showSaveDialog(this);
		
		if(choice == JFileChooser.APPROVE_OPTION)
			Report.PATH = fileChooser.getSelectedFile().getPath();
			
		else
			Test.log(Test.ERR, "Did not select path, resulting to default location");
	}
}
